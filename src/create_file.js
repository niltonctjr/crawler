const fs = require("fs");
const path = require("path");
const { path_create } = require("./utils/tools");

class CreateFile {
    constructor(file_name, content) {
        this._path = path.dirname(`${path.resolve("./output/")}${file_name}`);
        this.file_name = `${path.basename(`${path.resolve("./output/")}${file_name}`)}.md`;
        this._content = content;
    }

    run = () => {
        const fullname = `${this._path}/${this.file_name}`;
        path_create(this._path, () => {
            fs.writeFile(fullname, this._content, function (err) {
                if (err) throw err;
            });
        });
    };
}

module.exports = CreateFile;
