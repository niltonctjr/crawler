const HttpsProxyAgent = require("https-proxy-agent");
let axios = require("axios");

if (process.argv.length > 2 && process.argv[2].includes("--proxy")) {
    const configs = process.argv[2].replace("--proxy[", "").replace("]", "").split(",");

    const httpsAgent = new HttpsProxyAgent({ host: configs[0], port: configs[1], auth: configs[2] });
    axios = axios.create({ httpsAgent });
}

async function fetch_page(url) {
    let response = await axios(url).catch((err) => console.log(err));
    if (response === undefined) {
        console.log(`not response: ${url}`);
        return;
    }

    if (response.status !== 200) {
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}

module.exports = fetch_page;
