const fs = require("fs");
const cliProgress = require("cli-progress");
const colors = require("colors");

const path_create = (path, cb = null) => {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true }, (err) => {
            if (err) throw err;
        });
    }
    if (cb) cb();
};

const progress = (title) => {
    return new cliProgress.SingleBar({
        format: `${title} ${colors.cyan("{bar}")} {percentage}% | {eta}s | {duration}s | {value}/{total}`,
        barCompleteChar: "\u2588",
        barIncompleteChar: "\u2591",
        hideCursor: true,
        barsize: 50,
        position: "center",
    });
};

const header_markdown = (title, description, date, tags, thumbnail = "") => {
    return `---
title: ${title}
date: "${date.toISOString()}"
description: ${description}
published: false
${thumbnail.length > 0 ? ("thumbnail: " + thumbnail) : ""}
tags: [${tags.join(",")}]
---
`

}

module.exports = {
    path_create: path_create,
    progress: progress,
    header_markdown: header_markdown,
};
