// main.js
const fetch_page = require("./utils/fetch_page");
const fiaformula3_news = require("./sites/fiaformula3_news");
const fiaformula2_news = require("./sites/fiaformula2_news");

const pages = [fiaformula3_news, fiaformula2_news];
//const pages = [fiaformula3_news];

const main = async () => {
    console.log("Iniciado processo de crawler");
    for (let i = 0; i < pages.length; i++) {
        const page = pages[i];
        console.log(`\n${page.URL()}\n`);
        await fetch_page(page.URL()).then((res) => page.RES(res.data));
    }
};
main();
