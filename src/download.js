let request = require("request");
const fs = require("fs");
const path = require("path");
const cheerio = require("cheerio");
const mime = require("mime-types");
const { path_create } = require("./utils/tools");

if (process.argv.length > 2 && process.argv[2].includes("--proxy")) {
    const configs = process.argv[2].replace("--proxy[", "").replace("]", "").split(",");
    request = request.defaults({ 'proxy': `http://${configs[2]}@${configs[0]}:${configs[1]}` });
}

const doHead = (url) => {
    return new Promise((resolve, reject) => {
        request.head(url, (err, res, body) => {
            if (!err && res.statusCode === 200) resolve(res, body);
            else reject(err);
        });
    });
};

const donwload_img = async (imgs, path_output, imgname = "") => {
    const imgs_path = [];
    for (let i = 0; i < imgs.length; i++) {
        const img = imgs[i];
        const $ = cheerio.load(imgs[i]);
        const img_url = $(img).attr("loading") === "lazy" ? $(img).attr("data-src") : img.attribs.src;
        if (/^https?:\/\//.test(img_url)) {
            let img_name = imgname.length > 0 ? imgname : path.basename(img_url)

            const response = await doHead(img_url).catch(e => console.log(e));
            if (response.headers && response.headers["content-type"]) {
                img_name = img_name.includes(".")
                    ? img_name
                    : `${img_name}.${mime.extension(response.headers["content-type"])}`;
                path_create(path_output);
                request(img_url).pipe(fs.createWriteStream(`${path_output}/${img_name}`));
                imgs_path.push(img_name);
            }
        }
    }
    return imgs_path;
};
module.exports = donwload_img;
