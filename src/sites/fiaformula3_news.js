const cheerio = require("cheerio");
const path = require("path");
const fetch_page = require("../utils/fetch_page");
const create_file = require("../create_file");
const download_img = require("../download");
const TurndownService = require("turndown");
const TurndownPluginImageWithStyle = require("turndown-plugin-image-with-style");
const { progress, header_markdown } = require("../utils/tools");

const config_markdown = {
    headingStyle: "atx",
    codeBlockStyle: "fenced",
};

const res_news = async (file_name, data) => {
    const $ = cheerio.load(data);
    const title = $(".f1-article--title h1").text()
    const date = $(".f1-article--date").html();
    const tags = $(".f1-tag-list").first().find("li a").toArray();
    const body = `<h1>${title}</h1>${$(".f1-article--body").html()}`;
    const description = $(body).find("p").first().html()
    const imgs = $(body).find("img").toArray();

    const turndownService = new TurndownService(config_markdown);
    turndownService.use(TurndownPluginImageWithStyle);
    let markdown = turndownService.turndown(body);

    const imgs_filename = await download_img(
        imgs,
        `${path.resolve("./output")}/fiaformula3/${path.basename(file_name)}`
    );
    imgs_filename.forEach((img) => {
        markdown = markdown.replace("![](null){width=1000}", `![](./${img})`);
    });

    markdown = header_markdown(title, description, new Date(date), tags.map(t => `"${$(t).text()}"`)) + markdown
    new create_file(`/fiaformula3/${path.basename(file_name)}/index`, markdown).run();
};

class fiaformula3_news {
    constructor() {
        this._url_base = "https://www.fiaformula3.com";
        this._url_list = `${this._url_base}/Latest?filters=Guest%20Column%2CNews%2CReport%2CInterview%2CPreview%2CFeature`;
    }

    URL = () => {
        return this._url_list;
    };
    RES = async (data) => {
        const fiaformula3_progress = progress(" > progress");
        const html = data;
        const $ = cheerio.load(html);

        const itens = $(".article-listing-card--item a");
        fiaformula3_progress.start(itens.length, 0);
        for (let i = 0; i < itens.length; i++) {
            let link = $(itens[i]).attr("href");
            let imgs = $(itens[i]).find("img").toArray();
            await download_img(imgs, `${path.resolve("./output")}/fiaformula3/${path.basename(link)}`, "thumbnail");
            await fetch_page(`${this._url_base}${link}`).then((res) => res_news(link, res.data));
            fiaformula3_progress.increment();
            fiaformula3_progress.update(i + 1);
        }
        fiaformula3_progress.stop();
    };
}

module.exports = new fiaformula3_news();
